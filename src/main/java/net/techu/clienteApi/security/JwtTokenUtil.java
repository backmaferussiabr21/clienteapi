package net.techu.clienteApi.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import io.jsonwebtoken.Claims;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtTokenUtil implements Serializable {

    public static final long JWT_TOKEN_VALIDITY= 60*1000;

    @Value("${jwt.secret}")
    private String secreto;

    public <T> T getClainFromToken(String token, Function<Claims, T> claimsResolver){
        final Claims clains = getAllClaimsFromToken(token);
        return claimsResolver.apply(clains);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secreto).parseClaimsJws(token).getBody();
    }

    public Boolean isTokenExpired(String token){
        final Date expiration = getExpirationDateFromTolen(token);
        return expiration.before(new Date());
    }

    public Date getExpirationDateFromTolen(String token) {
        return getClainFromToken(token,Claims::getExpiration);
    }

    public String getUsernameFromToken(String token){
        return getClainFromToken(token, Claims::getSubject);
    }

    public Date geIssuedAtDateromToken(String token){
        return getClainFromToken(token, Claims::getIssuedAt);
    }

    /* Generar token */
    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return doGenerateToken(claims, userDetails.getUsername());
    }

    private String doGenerateToken(Map<String, Object> claims, String subject)
    {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY*1000))
                .signWith(SignatureAlgorithm.HS512, secreto).compact();
    }

}
