package net.techu.clienteApi.controller;

import net.techu.clienteApi.model.Usuario;
import net.techu.clienteApi.security.JwtTokenUtil;
import net.techu.clienteApi.service.JwtUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
public class UsuarioController {


    @Autowired
    private JwtUserDetailService jwtUserDetailService;


    @Autowired
    private JwtTokenUtil jwtTokenUtil;


    @GetMapping(value = "/usuarios/{usr}",produces = "application/json")
    public ResponseEntity<Usuario> getUser(@PathVariable String usr){
        final UserDetails userdeatails = jwtUserDetailService.loadUserByUsername(usr);
        final String token = jwtTokenUtil.generateToken(userdeatails);
        System.out.println("Token" +token);
        RestTemplate template = new RestTemplate();
        Usuario[] usuario = template.getForObject("http://localhost:8081/equipo4/usuarios",Usuario[].class,token);
        return new ResponseEntity<Usuario>(HttpStatus.OK);
    }
}
