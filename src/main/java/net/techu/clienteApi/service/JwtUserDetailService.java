package net.techu.clienteApi.service;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class JwtUserDetailService implements UserDetailsService {


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        if("Equipo4".equals(s)){
            return new User("Equipo4","4321",new ArrayList<>());
        }
        throw new UsernameNotFoundException(String.format("Usuario %s no encontrado",s ));
    }
}
