FROM openjdk:15-jdk-alpine
COPY target/clienteApi-0.0.1-SNAPSHOT.jar appapi.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/appapi.jar"]
